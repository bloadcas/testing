#Creadors de documentació

Apart dels coneguts programes d'ofimàtica com Office (MS-Word), OpenOffice (OO-Write), etc 
teniu a la vostra disposició llenguatges que a l'interpretar-se poden generar documentació
com aquesta (md markdown) o un pdf (LaTex).

---

### Markdown de GT
Si voleu crear documentació ràpida no cal que us hi trenqueu el cap intentant que quedi bonic.
La documentació ha de ser atractiva per ser llegida amb facilitat però la seva missió és que 
sigui útil.

En el món de la programació han aparegut editors de text de variades formes i aspectes; però
hi ha editors que han guanyat reputació de tal manera que s'haurien de conèixer bé. Un d'ells
és el **Markdown de GIT**. Un editor senzill que permet crear petites documentacions d'un software
o manuals senzills.

  * [Petit txuletari de la sintaxi de MD](https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf)
> Com a opinió personal, és molt bona idea fer una recopilació de cheatsheet (txuletaris, 
> minimanuals, etc) de tots els llenguatges que useu per poder recordar la seva sintaxi o com s'usa
> en tot moment.

---

### LaTex
Aquest programa (o interpretador de textos) és molt més complex que l'anterior. Permet crear 
documentació més elaborada. No és un editor WYSIWYG, és un editor enfocat al text i no a la presentació.
Per poder usar cal descarregar-se un editor de **LaTex** o bé el seu compilador. És un llenguatge que
conté símbols i expressions que permet formatar el text mentre s'escriu, per tant cal compilar el 
document per obtenir un PDF resultant.

  * [Descàrrega de **TexWorks** v.0.4.5 (http://www.tug.org/texworks)](http://www.tug.org/texworks/)

Si voleu manuals del llenguatge **LaTex**

  * [Introduction to LaTex (pdf)](http://www.docs.is.ed.ac.uk/skills/documents/3722/3722-2014.pdf)

  * [Not too short introduction to LaTex (pdf)](https://tobi.oetiker.ch/lshort/lshort.pdf)

  * [LaTex mathematical symbols (pdf)](https://www.caam.rice.edu/~heinken/latex/symbols.pdf)


---
_Copyright 2013 - Pitu - Bloadcas_

THE SOFTWARE (AND DOCUMENTATION) IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


