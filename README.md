# Breus apunts per a Becaris

En aquesta secció hi trobareu documentació variada que s'ha hagut de fer servir durant les sessions dels
becaris.

---

## Index

  0. [Creadors de documentació](dbt000_wordinterpreter.md)
     * Apart dels coneguts programes d'ofimàtica com Office (MS-Word), OpenOffice (OO-Write), etc 
       teniu a la vostra disposició llenguatges que a l'interpretar-se poden generar documentació
       com aquesta (md markdown) o un pdf (LaTex).
  1. [Varis d'Android](dbt001_android_string.md)
     * Documentació variada sobre Android.
  2. [Patrons de disseny](dbt002_design_patrons.md)
     * En la programació Orientada en Objectes pot ser interesant conèixer diferents patrons que 
       s'estableixen com a normes generals a seguir per obtenir uns objectius.
     * Patró Singleton, Patró Façana, Patró Observador, Patró Iterador
  3. [Calendaris](dbt003_calendar.md)
     * En la programació el tractament de les dates és un problema important. 
     * Aquí no es dóna cap solució i només s'hi aporten idees.
  4. [Punters](dbt004_pointers.md)
     - En OOP no hi ha punters però conèixe'ls és importantíssims.
     - Manual de C.
  5. [Base de dades - SQLite](dbt005_sqlite.md)
     - Detalls de SQLite 
  6. [Depurado - DEBUG](dbt006_debug.md)
     - Com depurar.
  7. [Les metadates](dbt007_metadata.md)
     - Quan connectem dos sistemes informàtics haurem d'usar dades d'intercanvi d'informació.
       Aquí explicarem que són les metadates i quins formats hi ha.
     - XML vs Json.
     - Yaml.
  8. [Tipus de dades](dbt008_datatypes.md)
     - Tipus de dades. Màxims i mínims.
  9. [Request For Comments](dbt009_rfc.md)
     - Documentació pública sobre components informàtics. 
     - Molt important que pocs conèixen.
   
---

## Algorismes que s'han de conèixer.

  - Recorreguts en un array. 
    - Suma dels elements d'un array.
    - Mitjana dels elements d'un array.
  - Cerca en un array.
    - Cerca en un array desordenat.
    - Cerca en un array ordenat.
  - Ordenació d'un array.
  - Fusió (merge) d'un array.
  - Recursivitat.
    - Factorial d'x (x!)

### Problemes famosos 

- Les torres de Hanoi
- La funció de 3n+1
- El problema de Josephus.

